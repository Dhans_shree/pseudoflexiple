import { Component, OnInit } from '@angular/core';
import {UserRegistrationService} from "../../../service/user-registration.service";
import {CognitoCallback} from "../../../service/cognito.service";
import {DropdownModule} from "ngx-dropdown";


import { ActivatedRoute, Params, Router } from '@angular/router';
import { FormGroup, FormControl, FormArray, Validators ,FormsModule,ReactiveFormsModule} from '@angular/forms';

export class RegistrationUser {
    name: string;
    email: string;
    password: string;
}

@Component({
  selector: 'freelancer-registraion-app',
  templateUrl: './registration.component.html'
})
export class RegisterComponent implements OnInit {
  errorMessage: string;
  registrationForm: FormGroup;
  registrationUser: RegistrationUser;
  
  constructor(private route: ActivatedRoute,
			  public userRegistration: UserRegistrationService, 
              private router: Router) {
  }

  ngOnInit() {
    this.initForm();
  }

  onSubmit() {
		this.registrationUser.name = this.registrationForm.value['fullname'];
		this.registrationUser.email = this.registrationForm.value['emailaddress'];
		this.registrationUser.password = this.registrationForm.value['password'];
        this.userRegistration.register(this.registrationUser, this);
    }

    cognitoCallback(message: string, result: any) {
		
        if (message != null) { //error
            this.errorMessage = message;
            console.log("result: " + this.errorMessage);
        } else { //success
            //move to the next step
            console.log("redirecting");
            this.router.navigate(['/home/confirmRegistration', result.user.username]);
        }
    }

	
  

  onCancel() {
    this.router.navigate(['../'], {relativeTo: this.route});
  }

  private initForm() {
    let fullname = '';
    let password = '';
	let emailaddress ='';
	this.registrationUser = {
		name: "",
		email: "",
		password: "",
	};
    this.registrationForm = new FormGroup({
      'fullname': new FormControl(fullname, Validators.required),
      'password': new FormControl(password, Validators.required),
	  'emailaddress': new FormControl(emailaddress, Validators.required)
    });
  }

}
