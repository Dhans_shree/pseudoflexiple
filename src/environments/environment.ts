export const environment = {
    production: false,
    region: 'us-east-1',
    identityPoolId: '',
    userPoolId: 'us-east-1_s4eGuh7Rw',
    clientId: '11kth6d7vfg0nsjqsksals34mj',

    rekognitionBucket: 'rekognition-pics',
    albumName: "usercontent",
    bucketRegion: 'us-east-1',

    ddbTableName: 'LoginTrail',

    cognito_idp_endpoint: '',
    cognito_identity_endpoint: '',
    sts_endpoint: '',
    dynamodb_endpoint: '',
    s3_endpoint: ''

};

